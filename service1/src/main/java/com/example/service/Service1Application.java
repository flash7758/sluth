package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

@RestController
@SpringBootApplication
public class Service1Application {

	public static void main(String[] args) {
		SpringApplication.run(Service1Application.class, args);
	}

	@Autowired
	private RestTemplate restTemplate ;

	@GetMapping("/test")
	public Mono<String> test(){
		return Mono.just("AAAAAAAA");
	}

	@GetMapping("/test2")
	public Mono<String> test2(){
		String a = restTemplate.getForObject("http://localhost:9413/test", String.class);
		return Mono.just(a);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
